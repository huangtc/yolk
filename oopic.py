#!/bin/python
#coding: utf-8

# 煎蛋妹子图（jandan ooxx）批量下载脚本 Python 版
# 作者： Tristan Huang huangtc AT outlook DOT com

from urllib import urlretrieve,urlopen
import re,sys,argparse

# 获取页面文件
def get_page(url):
    result = urlopen(url).read()
    return result

# 获取首页的页数
def get_last_pagenumber(page):
    print page
    result = re.findall(\
        r'<span class=\"current-comment-page\">\[(\d+)\]</span>',\
            get_page('http://jandan.net/'+page))[0]
    return int(result)

def download_images(start,end,size,all_flag,page_flag):
    imglist = []
    pagelist = []
    # 保证start值小于end
    if start > end:
        start,end=end,start
    # 获取要下载图片的页面
    for n in range(start,end+1):
        print n,
        pagelist.append(get_page('http://jandan.net/'+page_flag+'/page-'+str(n)))
    print '\n'
    # 然后逐页检查图片地址
    for page in pagelist:
        # 下载所有图床的图片（实验功能，不保证图片链接有效）
        if all_flag:
            imglist += re.findall(\
                r'<img src=\"(http://[\w\-./]+.jpg|png|jpeg|gif)\"\s+/>',page)
        # 仅下载新浪微博相册的图片
        else:
            imglist += re.findall(\
                r'<img src=\"(http://ww\d.sinaimg.cn/\w+/\w+.jpg|gif)\"\s+/>',page)
    # 下载图片
    for img in imglist:
        # 按照图片size设置替换url中相应的字段，仅用于新浪微博相册
        if all_flag:
            img = re.sub(r'/\w+/','/'+size+'/',img)
        print img
        urlretrieve(img,re.search(r'[\w\-]+.jpg|png|jpeg|gif$',img).group())

def main():
    parser = argparse.ArgumentParser(description='Yet Another Jandan image download script.')
    parser.add_argument('-f',action="store",dest="start",\
                            type=int,default=1,\
                            help='from X page start')
    parser.add_argument('-t',action="store",dest="end",\
                             type=int,default=1,\
                            help='to Y page end')
    parser.add_argument('-a',action="store_true",dest="all_flag",\
                            default=False,help='download all website image')
    parser.add_argument('-s',action="store",dest="size",\
                            default="large",choices=['small','bmiddle',\
                                                         'thumbnail',\
                                                         'mw600','large'],\
                            help='image size')
    parser.add_argument('-p',action="store",dest="page_flag",\
                            default="ooxx",choices=['ooxx','pic'],\
                            help='ooxx or pic')
    args = parser.parse_args()
    last_pagenumber = get_last_pagenumber(args.page_flag)
    # -f 和 -t 边界检查
    if args.start not in range(1,last_pagenumber+1) or \
            args.end not in range(1,last_pagenumber+1):
        print '-f START or -t END out of bounds'
        print 'please use a number between 1 and '+str(last_pagenumber)
        sys.exit(2)
    download_images(args.start,\
                        args.end,\
                        args.size,\
                        args.all_flag,\
                        args.page_flag)

if __name__ == '__main__':
    main()
